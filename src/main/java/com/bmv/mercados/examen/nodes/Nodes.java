package com.bmv.mercados.examen.nodes;

import com.bmv.mercados.examen.utils.Result;

public class Nodes {
	private static final int ABAJO = 0;
	private static final int DERECHA = 1;

	/**
	 * Encuentra nodos vecinos 
	 * @param columns
	 * @param rows
	 * @param lines
	 */
    public static void findNeighbors(int columns, int rows, String... lines) {
    	Character[][] malla = new Character[rows][columns];
    	
    	for (int row = 0; row < malla.length; row++) {    		
    		for (int column = 0; column < malla[row].length; column++) {
    			malla[row][column] = lines[row].charAt(column);
    		}
    	}
    	
    	for (int row = 0; row < malla.length; row++) {    		
    		for (int column = 0; column < malla[row].length; column++) {
    			if (malla[row][column] == '0') {
    				String data = column + "," + row + " ";
    				data += checaVecinos(row, column, malla, DERECHA) + " ";    				
    				data += checaVecinos(row, column, malla, ABAJO);
    				Result.add(data);
    			}
    		}
    	}
        //Para agregar las respuestas use:
        //Resul.add("");
    }
    
    /**
     * Checa nodos vecinos tanto abajo como a la derecha
     * @param x
     * @param y
     * @param malla
     * @param direccion
     * @return Posicion del nodo
     */
    private static String checaVecinos(int x, int y, Character[][] malla, int direccion) {
    	try {
    		if (direccion == ABAJO) {
		    	if (malla[x + 1][y] == '0') {
		    		return y + "," + (x + 1);
		    	}else {
		    		return checaVecinos(x + 1, y, malla, ABAJO);
		    	}    			
    		}
    		if (direccion == DERECHA) {    			
		    	if (malla[x][y+ 1] == '0') {
		    		return (y + 1) + "," + x;
		    	}else {
		    		return checaVecinos(x, y + 1, malla, DERECHA);
		    	}
    		}
    	}catch(Exception exc) {
    		return "-1,-1";
    	}
    	return "-1,-1";
    }    
}
