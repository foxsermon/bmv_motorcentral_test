package com.bmv.mercados.examen.defence;

public class FormatValidator {
	
    /**
     * Valida la cadena
     * @param format
     * @return True | False
     */
    public boolean validateFormat(String format) {
    	if (format == null 
    			|| format.length() == 0 
    			|| format.length() % 2 != 0
    			|| checaPrimerItem(format)) {
    		return false;
    	}
    	
    	return buscaBalance(format);
    }
    /**
     * Busca un que todo este balanceado
     * @param format
     * @return True | False
     */
    private boolean buscaBalance(String format) {
    	Character cerrar = buscaCerrado(format.charAt(0));
    	int indice = format.indexOf(cerrar);
    	
    	String sub1 = format.substring(1, indice);
    	String sub2 = format.substring(indice + 1);
    	String strTmp = sub1 + sub2;

    	if (strTmp.length() == 0) {
    		return true;
    	}
    	if (checaPrimerItem(strTmp)) {
    		return false;
    	}
    	return buscaBalance(strTmp);    	
    }
    /**
     * Checa primer elemento
     * @param format
     * @return True | False
     */
    private boolean checaPrimerItem(String format) {
    	return format.charAt(0) == '}'
    			|| format.charAt(0) == ']'
    			|| format.charAt(0) == ')';    	
    }
    /**
     * Busca su contraparte
     * @param abierto
     * @return Elemento cerrar
     */
    private Character buscaCerrado(Character abierto) {
    	if (abierto == '[') {
    		return ']';
    	}
    	if (abierto == '{') {
    		return '}';
    	}
    	if (abierto == '(') {
    		return ')';
    	}
    	return null;
    }
}
