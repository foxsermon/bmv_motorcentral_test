package com.bmv.mercados.examen.links;

public class OperationLinkedNode {
	
	/**
	 * Borra nodo cuando su valor sea igual a value
	 * @param node
	 * @param value
	 * @return Regresa nodos restantes
	 */
    public LinkedNode removeNodes(LinkedNode node, int value) {
    	if (node == null) {
    		return null;
    	}
    	if (node.next == null) {
    		return node.getValue() == value ? null : node;
    	}
    	if (node.getValue() == value) {
    		return removeNodes(node.next, value);
    	}
    	if (node.next.getValue() == value) {
    		node.next = node.next.next;
    	}    	
    	node.next = removeNodes(node.next, value);
    	
    	return node.getValue() == value ? null : node;
    }
}
