package com.bmv.mercados.examen.links;

public class LinkedNode {
    //Valor del nodo
    private int value;

    //Referencia al siguiente Nodo
    public LinkedNode next;

    public LinkedNode(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}
