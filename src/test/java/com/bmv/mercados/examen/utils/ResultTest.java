package com.bmv.mercados.examen.utils;

import java.util.List;

public class ResultTest extends Result {

    public static ResultTest CONSOLE = new ResultTest();

    @Override
    public List<String> getResults() {
        return Result.instance.getResults();
    }

    @Override
    public void reset() {
        Result.instance.reset();
    }
}